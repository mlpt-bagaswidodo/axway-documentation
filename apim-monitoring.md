# Enable monitoring Axway 

## Pengaturan di server 
1. install mysql
	
    #### Online
	- Download rpm mysql yum repo 

    ```
    wget https://repo.mysql.com//mysql80-community-release-el7-3.noarch.rpm
    ```

	- Install rpm
    ```
    sudo yum localinstall mysql80-community-release-el7-3.noarch.rpm
    ```

	- Install mysql
    ```
    yum install mysql-community-server
    ```

	- start mysql
    ```
    systemctl start mysqld
    ```

	- cek generated password cek log di ``/var/log/mysqld.log``
	- ubah password root 
    ```
    ALTER USER 'root'@'localhost' IDENTIFIED BY 'r00?R00t';
    ```

    #### Offline
    - download versi compress versi mysql +- 500mb dalam bentuk ``*.tar``
    - cari versi mariadb lib terinstall
	    ```
        rpm -qa | grep mariadb-libs mariadb-libs
        ```
    - hapus mariadb lib
        ```
        sudo yum remove mariadb-libs-5.5.44-2.el7.centos.x86_64
        ```
    - install mysql server rpm
        ```
        rpm -ivh mysql-community-common-5.7.21-1.el7.x86_64.rpm
        rpm -ivh mysql-community-libs-5.7.21-1.el7.x86_64.rpm
        rpm -ivh mysql-community-client-5.7.21-1.el7.x86_64.rpm
        rpm -ivh mysql-community-server-5.7.21-1.el7.x86_64.rpm
        ```

2. tambahkan jar mysql connector di  ``<install dir>/apigateway/ext/lib/``

3. restart apigateway + instance
    1.  stop instance
    ```
    ./startinstance -g "<GROUP_NAME>" -n "<INSTANCE_NAME>" -d
    ```
    2. stop nodemanager
    ```
    ./nodemanager -k
    ```


    3. start instance
    ```
    ./startinstance -g "<GROUP_NAME>" -n "<INSTANCE_NAME>" -d
    ```
    4. start nodemanager
    ```
    ./nodemanager -d
    ```

4. buat database di mysql contoh `` reports77 ``
    ```
        create database reports77
    ```
5. initialize schema dengan command ``dbsetup`` pada direktori ``<install dir>/apigateway/posix/bin/`` 
    ```
	./dbsetup --dburl=jdbc:mysql://127.0.0.1:3306/reports77 --dbuser=root --dbpass=r00?R00t
    ```
6. enable monitoring dengan command ``managedomain`` pada direktori ``<install dir>/apigateway/posix/bin/`` 

	- jika belum ada topology
	```
    ./managedomain --initialize --metrics_enabled y --metrics_dburl jdbc:mysql://127.0.0.1:3306/reports77 --metrics_dbuser <userdb> --metrics_dbpass <passdb>
    ```
	
	- jika sudah ada topology sebelumnya
    ```
	./managedomain --edit_host  --metrics_enabled y --metrics_dburl jdbc:mysql://127.0.0.1:3306/axway_metrics_77 --metrics_dbuser <userdb> --metrics_dbpass <passdb>
    ```


## Pengaturan di Policystudio

1.  Tambahkan connector mysql di policy studio pada menu ``
	Window > Preferences > Runtime Dependencies``

    <img src="assets/enable-monitoring/00-runtime-dependencies.PNG" alt="drawing" />
2. restart policystudio menggunakan command prompt dengan tambahan parameter -clean
    ```
    policystudio.exe -clean
    ```
3. Konfigurasi koneksi apigateway ke mysql database
    <img src="assets/enable-monitoring/01-configure-db-conn.PNG" alt="drawing" />
4. Configurasi logging ke database
<img src="assets/enable-monitoring/03-logging-db.PNG" alt="drawing" />

5. Enable monitoring api manager
<img src="assets/enable-monitoring/02-enable-monitoring-api-manager.PNG" alt="drawing" />
6. Deploy ke api gateway


## Monitor di api manager

1. Berdasarkan API
<img src="assets/enable-monitoring/04-all-api.PNG" />
2. Berdasarkan Organization
<img src="assets/enable-monitoring/05-by-organization.PNG" />
	
	

		
	

	
