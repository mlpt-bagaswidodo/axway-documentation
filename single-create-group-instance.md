# Konfigurasi Instance dan Grup

### Registrasi Host

1. Masuk ke direktor `<install dir>/apigateway/posix/bin`
2. Jalankan perintah `./managedomain --menu`
    ```
    # ./managedomain --menu
    Enter username: admin
    Enter password: ********
    -------------------------------------------------------
                    Manage Domain Menu
    -------------------------------------------------------
    Admin Node Manager: https://localhost:8090


    Host Management:
    1) Register host
    2) Edit a host
    3) Delete a host
    4) Change Admin Node Manager and/or credentials, currently connecting as:
            user 'admin' with truststore 'None'
    API Gateway Management:
    5) Create API Gateway instance
    6) Edit API Gateway (i.e., rename, change management port)
    7) Delete API Gateway instance
    8) Add a tag to API Gateway
    9) Delete a tag from API Gateway
    10) Add init.d script for existing local API Gateway
    Group Management:
    11) Edit group (i.e., rename)
    12) Delete a group
    Topology Management:
    13) Print topology
    14) Check topologies are in sync
    15) Check the Admin Node Manager topology against another topology
    16) Sync all topologies
    17) Reset the local topology
    Deployment:
    18) Deploy to a group
    19) List deployment information
    20) Create deployment archive
    21) Download deployment archive
    22) Update deployment archive properties
    23) Change group configuration passphrase
    Domain SSL certificates:
    24) Regenerate SSL certificates on localhost
    25) Sign Certificate Signing Request (CSR)
    26) Submit externally signed certificate


    q) Quit


    ```

3. Pilih no 1
    ```
    Select option: 1
    Is this the first host (Admin Node Manager) in the domain [y]: y
    Enter details of the new host to be registered...
    Select/enter local hostname or IP address:
    1) localhost
    2) localhost
    3) 127.0.0.1
    4) Enter hostname or IP address
    Enter selection from 1-4 [1]:
    Enter port [8090]:
    Enter Node Manager name [Node Manager on localhost]:
    Select option for certificate management for internal SSL communications:
        1) Use system generated CA key and certificate to sign all SSL certificates
        2) Use user provided CA key and certificate to sign all SSL certificates
        3) All SSL certificates must be signed by an external CA
    Enter selection from 1-3 [1]:
    Enter passphrase for domain CA private key [none]:
    Reenter passphrase for domain CA private key [none]:
    Enter domain identifier to be used in dname of system generated domain certificate [Domain]:
    Enter signing algorithm for certificates [sha256]:
    Enter passphrase for temporary key files stored on disk [none]:
    The default subject alternative names for the Node Manager's certificate are:

    DNS.1 = localhost
    IP.1 = 127.0.0.1

    Accept the default subject alternative names [y]:
    Do you want to create an init.d script for this Node Manager [n]:
    Do you wish to edit metrics configuration (y or n) ? [n]:

    ```
3. Keluar dulu dari menu
    ```
    Select option: q
    ```
4. Start node manager `./nodemanager -d`
5. Cek apakah port 8090 (Default port apigateway) sudah listen
    ```
    # netstat -tlpn | grep 8090
    tcp        0      0 0.0.0.0:8090            0.0.0.0:*               LISTEN      5701/Node Manager o
    ```
6. Buka browser dan coba hit `https://<ipserver>:8090`
<img src="assets/create-group-instance/single-create-group-instance-00.PNG" alt="browser" />
7. Login Dengan default credential 
    - username : admin 
    - password : changeme
    <img src="assets/create-group-instance/single-create-group-instance-01.PNG" alt="browser" />

### Buat instance dan grup
1. Masuk ke direktor `<install dir>/apigateway/posix/bin`
2. Jalankan perintah `./managedomain --menu`
3. Pilih opsi 5
    ```
    Select option: 5
    ```
4. Inputkan  nama group dan instance
    - Group : Test Group
    - Instance  : Test Instance

    ```
    Enter API Gateway name: TEST-INSTANCE
    Enter group name: TEST-GROUP
    Select a host:
    1) localhost
    2) Enter host name
    Enter selection from 1-2 [localhost]:
    Enter local management port for instance [8085]:
    Enter external traffic port for instance [8080]:
    Do you want to create an init.d script for this instance [n]:
    Select option for certificate management for internal SSL communications:
    1) Use system generated CA key and certificate to sign all SSL certificates
    2) Use user provided CA key and certificate to sign all SSL certificates
    3) All SSL certificates must be signed by an external CA
    Enter selection from 1-3 [1]:
    Enter passphrase for domain CA private key [none]:
    Enter signing algorithm for certificates [sha256]:
    Enter passphrase for temporary key files stored on disk [none]:
    ```
5. Start instance dengan perintah
    ```
    ./startinstance -g "TEST-GROUP" -n "TEST-INSTANCE" -d
    ```
6. Cek di browser, jika hijau berarti sudah run dengan baik
<img src="assets/create-group-instance/single-create-group-instance-02.PNG" alt="browser" />