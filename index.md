# Axway 101
![alt text](https://www.axway.com/themes/custom/axway2020/img/axway-logo-dark-gray.svg "Logo Title Text 1")


Table of contents
- Installation
    - Instalasi pada single node server
        - [Installation](./single-installation.md)    
        - [Buat Instance dan Grup](./single-create-group-instance.md)  
        - [Konfigurasi API Manager](./single-configure-api-manager.md)  
        - Install dan konfigurasi API Portal
        - Install Decission Insight

     - Installation pada Multi DC / Server
        - API Gateway
        - Cassandra

- Development
    - [Buat Simple API](./labsheet-01-create-simple-api.md)
    - [Scripting groovy / java ](./labsheet-02-scripting-language.md)
    - SOAP-JSON
    - JSON-XML
    - JSON Path
    - Data mapper
    - Penggunaan KPS Table
    - Expose API ke API Manager
    - Authentikasi
        - Oauth
        - API Key
    - Membuat simulator API
        - Mockstastic
        - Express js node js
        - Java spring boot
    - JWT
        - Generate JWT Token
        - Verifikasi JWT Token



- Additional
    - SSL Certificates
        - install certificates
        - renew certificates
        - generate private key
        - extract p12 to crt/pem
        - extract jwt 

    - [API Manager Monitoring](./apim-monitoring.md) 
    - api logging
    - system logging
    - configure mail 
    - Perubahan skema pada KPS
