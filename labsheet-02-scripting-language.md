# Scripting Language di Policy Studio

1. Buat Policy Baru
2. Tambahkan filter scripting language ke canvas
    <br />
    <img src="assets/02-labsheet-scripting/scripting-00.png">
    <br />
3. Pada form pada _**Language pilih : Groovy**_   pada textarea ketikkan script berikut
    ```
    import java.text.SimpleDateFormat;
    import java.util.Date;
    def invoke(msg) {
        def timestamp = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").format(new Date().getTime());
        msg.put("timestamp",timestamp);
    return true; }
    ```
4. Tambahkan filter set message, set **Content-Type: application/json** pada body ketikkan body sebagai berikut
    ```
    {
        "timestamp":"${timestamp}"
    }
    ```
5. Jangan Lupa tambahkan filter Reflect message `200` untuk memberikan respon sukses
5. Final Policy
    <br />
    <img src="assets/02-labsheet-scripting/scripting-01.PNG">
    <br />
6. Tambahkan policy pada listener
7. Deploy
8. Test menggunakan postman / curl. 
    ```
    $ curl http://localhost:8080/scripting
    {
            "timestamp":"2021-07-27T14:54:14"
    }
    ```
