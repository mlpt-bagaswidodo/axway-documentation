1. Fetch project dari server<br />
![](assets/configure-api-manager/configure-api-manager-00.PNG)<br />
2. Inputkan nama project <br />
![](assets/configure-api-manager/configure-api-manager-01.PNG)<br />
3. Masukkan alamat dan credential password<br />
![](assets/configure-api-manager/configure-api-manager-02.PNG)<br />
4. Pilih group dan instance<br />
![](assets/configure-api-manager/configure-api-manager-03.PNG)<br />
5. Pilih Configure api manager<br />
![](assets/configure-api-manager/configure-api-manager-04.PNG)<br />
6. Konfigurasi koneksi ke cassandra<br />
![](assets/configure-api-manager/configure-api-manager-05.PNG)<br />
7. Konfigurasi credential saja. password. jika ingin parameter lain seperti smtp bisa disesuaikan<br />
![](assets/configure-api-manager/configure-api-manager-06.PNG)<br />
8. Deploy ke server, inputkan host username dan password<br />
![](assets/configure-api-manager/configure-api-manager-07.PNG)<br />
9. Pilih group dan instance<br />
![](assets/configure-api-manager/configure-api-manager-08.PNG)<br />
10. Tunggu hingga deployment selesai<br />
![](assets/configure-api-manager/configure-api-manager-09.PNG)<br />
11. Cek port apimanager (Default : 8075)<br />
![](assets/configure-api-manager/configure-api-manager-10.PNG)<br />
12. Login dengan username password pada step 7. Ubah password jika diminta<br />
![](assets/configure-api-manager/configure-api-manager-11.PNG)<br />
13. Jika Berhasil maka akan di arahkan ke halaman API di api manager<br />
![](assets/configure-api-manager/configure-api-manager-12.PNG)<br />
