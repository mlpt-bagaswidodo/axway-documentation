# Membuat api REST sederhana di AXWAY dengan policy studio


1. Fetch project dari server<br />
![](assets/configure-api-manager/configure-api-manager-00.png)<br />
2. Buat container baru<br />
![](assets/01-labsheet-create-simple-api/simple-api-00.png)<br />
3. Buat container baru misa `labsheet`<br />
![](assets/01-labsheet-create-simple-api/simple-api-01.PNG)<br />
4. Buat Buat Policy baru dengan klik kanan pada container yang telah dibuat sebelumnya inputkan nama policy misal `01-Shiping Mockup`<br />
![](assets/01-labsheet-create-simple-api/simple-api-02.PNG)<br />
5. Pada palete filter pilih filet **Set Message** <br />
![](assets/01-labsheet-create-simple-api/simple-api-03.png)<br />
6. Pada pada properti  **Set Message**. inputkan parameter berikut.
    -   Content Type : `application\json`
    -   Message Body : 
        ```
        {
            "status" : "OK,
            "delivered" : "Jan 20, 2021  12:30:49 PM"
        }
         ```
         <br />
    ![](assets/01-labsheet-create-simple-api/simple-api-04.PNG)<br />
  2. Tambahkan filter baru  **Reflect Message**.  <br /> 
![](assets/01-labsheet-create-simple-api/simple-api-05.png)<br />
3. inputkan parameter berikut.
    - Name : (customized)
    - HTTP reseponse code status : `200` <br />
![](assets/01-labsheet-create-simple-api/simple-api-06.PNG)<br />
3. Drag n drop filter success path<br />
![](assets/01-labsheet-create-simple-api/simple-api-07.png)<br />
3. Klik kanan pada Set Message filter, pilih Set as Start<br />
![](assets/01-labsheet-create-simple-api/simple-api-08.png)<br />
1. konfigurasikan *path* pada default listener pada menu **Environement Configuration > Listeners > Api Gateway > Default Services > Paths**<br />
![](assets/01-labsheet-create-simple-api/simple-api-09.PNG)<br />
12. Pilih tombol **add**, pilih **Relative path**.<br />
![](assets/01-labsheet-create-simple-api/simple-api-10.png)<br />
13. pada input *When a request arrives that matches the path* inputkan path yang akan di hit nantinya misal **/shipping** <br />
![](assets/01-labsheet-create-simple-api/simple-api-11.PNG)<br />
14. Pada *Path spesific policy* pilih policy nantinya yang akan di eksekusi jika path tersebut di hit. <br />
![](assets/01-labsheet-create-simple-api/simple-api-12.PNG)<br />

## Deployment 
1. Deploy Ke Api Gateway pada menu di pojok kanan atas atu dengan shortcut **(f6)** bisa juga dari menu **Tasks > Deploy**<br />
![](assets/01-labsheet-create-simple-api/simple-api-14.png)<br />
2. Inputkan alamat server dan credentials apigateway. 
    - Host : **(alamat server)**
    - Port : **8090** (default)
    - username : **admin** (username default apigateway manager)
    - password : **changeme** (default password apigateway manager)<br />
![](assets/01-labsheet-create-simple-api/simple-api-15.PNG)<br />
3. Pilih Group dan instance dimana policy akan di deploy<br />
![](assets/01-labsheet-create-simple-api/simple-api-16.PNG)<br />
4. Jika completed dan successfull bisa finish<br />
![](assets/01-labsheet-create-simple-api/simple-api-17.PNG)<br />

## Test API
15. lakukan percobaan menggunakan curl atau postman untuk hit api yang telah dibuat<br />
![](assets/01-labsheet-create-simple-api/simple-api-13.PNG)<br />
