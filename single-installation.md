# Instalasi pada single server

1. Upload installer dan license ke server menggunakan sftp misal winscp ke `/home/user`
2. Ubah permission menjadi executable
    ```
    chmod +x APIGateway_7.7_Install_linux-x86-64_BN4.run
    ```
3. Jalankan installer
    ```
     ./APIGateway_7.7_Install_linux-x86-64_BN4.run
    ```
4. Setujui license. Install semua package mandatory
    - Admin Node Manager
    - API Gateway Server
    - API Manager
    - Cassandra
    - Package & Deploy Tools. 
    <hr />


    ```
    Do you accept this license? [y/n]: y
    Setup Type

    [1] Standard - All API Gateway components without API Manager: All product components including Quick Start Tutorial, Cassandra Server and Desktop Tools will be installed. API Manager will not be installed.

    [2] Complete -  All API Gateway components including API Manager: All product components including API Manager, Quick Start Tutorial, Cassandra Server and Desktop Tools will be installed.

    [3] Custom (choose if upgrading) - Select components: Select components as needed. If upgrading, you must use this option.
    Please choose an option [1] : 3

    ----------------------------------------------------------------------------
    Select the components you want to install; clear the components you do not want
    to install. Click Next when you are ready to continue.

    Admin Node Manager [Y/n] :y
    Cassandra [y/N] : y
    API Gateway Server [Y/n] :y
    API Gateway Server - Quick Start Tutorial [y/N] : n
    API Manager [y/N] : y
    API Gateway Analytics [y/N] : n
    Desktop Tools : Y (Cannot be edited)
    Desktop Tools - Policy Studio [Y/n] :n
    Desktop Tools - Configuration Studio [y/N] : n
    Desktop Tools - API Tester [y/N] : n
    Package & Deploy Tools [y/N] : y

    Is the selection above correct? [Y/n]: y
    Please specify the directory where Axway API Gateway will be installed.
    Installation Directory [/opt/Axway-7.7.0]: /home/user/Axway-7.7.0

    ----------------------------------------------------------------------------
    API Gateway License
    Please specify a license file for your selected components.
    File: []: /home/user/apim.lic

    ----------------------------------------------------------------------------
    Cassandra configuration
    You have selected the option to install Cassandra.
    Please specify a directory where the Cassandra server will be installed, and the
    location of the JRE to be used. It is recommended that a 64-bit JRE is used for
    Cassandra in production.

    Installation Directory: [/opt]: /home/user/

    JRE Location: [/home/user/Axway-7.7.0/apigateway/platform/jre]:

    Start Cassandra Server after install?

         [y/N]: n

    Select No if you are upgrading.

    ----------------------------------------------------------------------------
    Admin Node Manager credentials

    By default the user name and password for the administrator account for the
    domain is 'admin' with password 'changeme'. For security reasons, it is
    recommended that you change these settings.

    Change the default user name and password  [Y/n]: n

    ----------------------------------------------------------------------------
    Additional Information

    API Manager is about to be installed.

    Before starting API Manager, you must configure a group and an instance.
    Please consult the "API Management Guide" document for more information.


    ----------------------------------------------------------------------------
    Summary

    The following components will be installed:

    - Admin Node Manager
    - API Gateway Server
    - API Manager
    - Cassandra
    - Package & Deploy Tools


    ----------------------------------------------------------------------------
    Please wait while Setup installs packages on your computer.

    Installing
    0% ______________ 50% ______________ 100%
    #########################################

    ----------------------------------------------------------------------------
    Set up has finished installing on your computer.

    Refer to the Installation and Configuration Guide to configure the Axway API
    Gateway.


    ```

5. 